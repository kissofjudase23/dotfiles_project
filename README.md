# Description
 This project is used to provide basic development setting.
 1. bash shell setting (.bashrc)
 2. vim editor with several plugins (.vimrc) 
 <pre>
	a.cscope
	b.taglist
	c.vim-fugitive
	d.nerdtree
	e.syntastic
	f.YouCompleteMe
 </pre>
 3. screen setting (.screenrc)
 4. git config  (.gitconfig)

# Install dotfiles
<pre><code>source ./install.sh
</code></pre>

# Uninstall dotfiles
<pre><code>source ./un_install.sh
</code></pre>
